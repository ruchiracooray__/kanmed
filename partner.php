<?php include('header.php'); ?>

	<div class="business-cover-img">
		<div class="partner-cover"></div>
	</div>

	<div class="body-section">

		<div class="row">
			<div class="container">
				<div class="container-wrap">

					<div class="col-4 col-mb-12 business-head-section">
						<div class="business-head">
							<h1><span>our</span> partners</h1>
						</div>
					</div>

					<div class="col-8 col-mb-12 business-description-section">
						<div class="business-description">

							<div class="topic-description">
								<p>consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
								 exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
								 dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
								 laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, </p>
							</div>
						</div>
						
						<div class="partner-section">
								<ul class="partners-logo">
									<li>
										<img src="img/partner/logo/aoki.png" />
										<div class="partner-overlay"><a href="#">view <br> products</a></div>
									</li>
									<li>
										<img src="img/partner/logo/amed.png" />
										<div class="partner-overlay"><a href="#">view <br> products</a></div>
									</li>
									<li>
										<img src="img/partner/logo/coopersurgical.png" />
										<div class="partner-overlay"><a href="#">view <br> products</a></div>
									</li>
									<li>
										<img src="img/partner/logo/aspide.png" />
										<div class="partner-overlay"><a href="#">view <br> products</a></div>
									</li>
									<li>
										<img src="img/partner/logo/wego.png" />
										<div class="partner-overlay"><a href="#">view <br> products</a></div>
									</li>
									<li>
				
										<img src="img/partner/logo/staplelinemedizintechnik.png" />
										<div class="partner-overlay"><a href="#">view <br> products</a></div>
									</li>
									<li>
										<img src="img/partner/logo/Genadyne.png" />
										<div class="partner-overlay"><a href="#">view <br> products</a></div>
									</li>
									<li>
										<img src="img/partner/logo/fcaresystems.png" />
										<div class="partner-overlay"><a href="#">view <br> products</a></div>
									</li>
									<li>
										<img src="img/partner/logo/surgicalinnovations.png" />
										<div class="partner-overlay"><a href="#">view <br> products</a></div>
									</li>
									<li>
										<img src="img/partner/logo/subiton2.png" />
										<div class="partner-overlay"><a href="#">view <br> products</a></div>
									</li>
								</ul>	
						</div>
					</div>

				</div>
			</div>
		</div>

	</div>

<?php include('footer.php'); ?>	
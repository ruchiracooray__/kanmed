<?php include('header.php'); ?>

		


		<div class="map-section">
			<div class="google-map" id="map"></div>
		</div>

		<div class="body-section">
			<div class="row">
				<div class="container">
					<div class="container-wrap center">
							<h1>Contact Us</h1>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting <br> industry. Lorem Ipsum has been the industry's standard </p>
					</div>
				</div>
			</div>
			<div class="row conact-info-row">
				<div class="container">
					<div class="container-wrap">
						<div class="contact-info">
							<div class="contact-details address">
								<div class="details-wrap wrap-center">
									<span class="icon-contact icon-address"></span>
									<span class="contact-font">Address</span>
									<p>No. 35/1, Sumedha mawatha, Walpola, Mulleriyawa, Sri Lanka.</p>
								</div>
							</div>
							<div class="contact-details">
								<div class="details-wrap">
									<span class="icon-contact icon-telephone"></span>
									<span class="contact-font">Phone</span>
									<p>+94 11 2157357</p>
								</div>
							</div>
							<div class="contact-details">
								<div class="details-wrap">
									<span class="icon-contact icon-fax"></span>
									<span class="contact-font">Fax</span>
									<p>+94-11-2578387</p>
								</div>
							</div>
							<div class="contact-details">
								<div class="details-wrap">
									<span class="icon-contact icon-emal"></span>
									<span class="contact-font">Email</span>
									<p>info@kanmed.lk</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="container">
					<div class="container-wrap center">
						<h1>WORKING HOURS</h1>
						<div class="working-hours">
							<div class="days">
								<h3>Monday - Friday</h3>
								<p>10:00 am – 8:30 pm</p>
							</div>
							<div class="days days-dark">
								<h3>Saturday</h3>
								<p>8:00 am – 7:00 pm</p>
							</div>
							<div class="days">
								<h3>Sunday</h3>
								<p>Closed</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="container">
					<div class="container-wrap center">
							<h1>Write to Us If You Have Questions</h1>
						<div class="from-section">
							<form>
								<div class="form-inputs">
									<input type="text" name="form-name" placeholder="Name">
								</div>
								<div class="form-inputs">
									<input type="text" name="form-email" placeholder="Email">
								</div>
								<div class="form-inputs">
									<input type="text" name="form-phone" placeholder="Phone (Optional)">
								</div>
								<div class="form-inputs">
									<textarea type="text" placeholder="Message"></textarea>
								</div>
								<div class="form-inputs button-right">
									<input class="form-submit" type="submit" value="Send Message">
								</div>
							</form>
						</div>
					</div>
					
				</div>
			</div>

		</div>

<!-- added Google Maps -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDqXYAhrMxBcZm4jU9mlbiC3Utaoxnxw6w"></script>
	<script>
 
      var map;
		function initialize() {
		  map = new google.maps.Map(document.getElementById('map'), {
		    zoom: 16,
		    center: new google.maps.LatLng(6.918696, 79.925774),
		    mapTypeId: 'roadmap',
		    styles : [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]

		    
		  });

		  var iconBase = base+'/img/loc/';
		  var icons = {
		    logo: {
		      icon: iconBase + 'logo.png'
		    }
		  };

		  function addMarker(feature) {
		    var marker = new google.maps.Marker({
		      position: feature.position,
		      icon: icons[feature.type].icon,
		      map: map
		    });
		  }

		  var features = [
		  
		    {
		      position: new google.maps.LatLng(6.918696, 79.925774),
		      type: 'logo',
		    }
		    
		  ];

		  for (var i = 0, feature; feature = features[i]; i++) {
		    addMarker(feature);
		  }
		}

		google.maps.event.addDomListener(window, 'load', initialize);
    </script>

<?php include('footer.php'); ?>

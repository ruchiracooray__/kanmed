<?php include('header.php'); ?>

	<div class="body-section">
		<div class="hidden-head">
			<h1>Rumi II Handle</h1>
		</div>
		<div class="row">
			<div class="container">
				<div class="container-wrap">

					<div class="col-6 col-mb-12 details-wrap" >
						<div class="img-sub"  >
							<div  class="detail-img">
								<div class="overlay"></div>
								<a href="#">
									<img src="img/details/1.jpg">
								</a>
							</div>
							<div class="detail-img">
								<div class="overlay"></div>
								<a href="#">
									<img src="img/details/2.jpg">
								</a>
							</div>
							<div class="detail-img">
								<div class="overlay"></div>
								<a href="#">
									<img src="img/details/3.jpg">
								</a>
							</div>
							<div class="detail-img">
								<div class="overlay"></div>
								<a href="#">
									<img src="img/details/4.jpg">
								</a>
							</div>
						</div>
						<div class="img-main" >
							<a href="#">
								<img src="img/details/3.jpg">
							</a>			
						</div>
					</div>

					<div class="col-6 col-mb-12 details-para">
						<div class="description">
							<h1>Rumi II Handle</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
							Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis
							unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, </br>
							Totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
							Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>	
						</div>
						<div class="row">
							<div class="col-6 col-mb-12 video-wrap">
								<div class="video-inner">
									<video id="bgvid">
									  <source src="video/design.mp4">
									  Your browser does not support the video tag.
									</video>
									<div class="play-btn" id="vidpause"></div>

									<script type="text/javascript">
										$(document).ready(function(){
										var vid = document.getElementById("bgvid"),
											pauseButton = document.getElementById("vidpause");
											
											pauseButton.addEventListener("click", function() {
											    vid.classList.toggle("stopfade");
												if (vid.paused) {
											vid.play();
													//pauseButton.innerHTML = "Pause";
													$(this).addClass('btn-effect');
												} else {
											        vid.pause();
											       // pauseButton.innerHTML = "Paused";
											        $(this).removeClass('btn-effect');
												}
											});
										});
									</script>
								</div>
							</div>

							<div class="col-6 col-mb-12 button-wrap">
								<button class="dwnload-btn">
									<div class="btn-grid">
										<div class="hide-svg">
								            <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%">
								            <line class="top" x1="0" y1="0" x2="708" y2="0"/>
								            <line class="left" x1="0" y1="156" x2="0" y2="-104"/>
								            <line class="bottom" x1="236" y1="52" x2="-472" y2="52"/>
								            <line class="right" x1="236" y1="0" x2="236" y2="156"/>
								            </svg>
							        	</div>
										<p>Download Catelog</p>
									</div>
								</button>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>


		<div class="row">
			<div class="container">
				<div class="container-wrap">
					<div class="related-head">
						<h1>Related Products</h1>
					</div>

					<div class="col-12 col-tb-12 col-mb-12">
						<div class="related-products">
							<div class="related-wrap">
								<div class="overlay">

							        <div class="svg-wrapper">
							          <svg height="367px" width="255px" xmlns="http://www.w3.org/2000/svg">
							            <rect class="shape" height="367px" width="255px"/>
							          </svg>
							          	<div class="text">view <br> product</div>
							        </div>
									
								</div>
								<div class="related-inner">
									<div>
										<img src="img/products/1.jpg">
									</div>
									<div class="names">
										<a href="#" class="product-name">Cervical Sizer</a>
									</div>
								</div>	
							</div>
							<div class="related-wrap">
								<div class="overlay">
									
									<div class="svg-wrapper">
							          <svg height="367px" width="255px" xmlns="http://www.w3.org/2000/svg">
							            <rect class="shape" height="367px" width="255px"/>
							          </svg>
							          	<div class="text">view <br> product</div>
							        </div>

								</div>
								<div class="related-inner">
									<div>
										<img src="img/products/2.jpg">
									</div>
									<div class="names">
										<a href="#">HUMI (Harris-Kronner) <br> Uterine Manipulator/ <br> Injector</a>
									</div>
								</div>	
							</div>
							<div class="related-wrap">
								<div class="overlay">

									<div class="svg-wrapper">
							          <svg height="367px" width="255px" xmlns="http://www.w3.org/2000/svg">
							            <rect class="shape" height="367px" width="255px"/>
							          </svg>
							          	<div class="text">view <br> product</div>
							        </div>

								</div>
								<div class="related-inner">
									<div>
										<img src="img/products/3.jpg">
									</div>
									<div class="names">
										<a href="#">SeeClear Surgical <br> Smoke Evacuation System</a>
									</div>
								</div>	
							</div>
							<div class="related-wrap">
								<div class="overlay">
									
									<div class="svg-wrapper">
							          <svg height="367px" width="255px" xmlns="http://www.w3.org/2000/svg">
							            <rect class="shape" height="367px" width="255px"/>
							          </svg>
							          	<div class="text">view <br> product</div>
							        </div>

								</div>
								<div class="related-inner">
									<div>
										<img src="img/products/4.jpg">
									</div>
									<div class="names">
										<a href="#">Apple-Hunt Laparascopic <br> Cannula/Trocar</a>
									</div>
								</div>	
							</div>

						</div>
					</div>

				</div>
			</div>
		</div>

	</div>

<?php include('footer.php'); ?>	
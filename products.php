<?php include('header.php'); ?>

	<div class="cover-section other-section ">
		<div class="about-section products-section">
			<div class="container">
				<h1>Kanmed Products</h1>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. <br> 
					Lorem Ipsum has been the industry's standard dummy text ever since the <br> 
					1500s, when an unknown printer took a galley of type and scrambled it to make a type </p>	
			</div>
		</div>
	</div>

	<div class="body-section">
		<div class="row">
			<div class="container">
				<div class="container-wrap">
					<div class="col-3 col-tb-4 col-mb-12">
						<div class="filter-products">
							<div class="filter-heading">
								<span>FILTER BY</span>
							</div>
							<div class="filter-cat">
								<span class="filter-name">BRANDS</span>
								<ul class="myclass">
									<li><a href="#">SeeClear</a></li>
									<li><a href="#">Apple-Hunt</a></li>
									<li><a href="#">Lone Star</a></li>
									<li><a href="#">Mobius</a></li>
									<li><a href="#">Zeppelin</a></li>
									<li><a href="#">Other</a></li>
									<li><a href="#">Koh-Efficient</a></li>
									<li><a href="#">Plume-Away</a></li>
									<li><a href="#">Other Brands</a></li>
								</ul>
								<br>

								<span class="filter-name">TYPE</span>
								<ul class="myclass">
									<li><a href="#">Instruments</a></li>
									<li><a href="#">Accessories</a></li>
									<li><a href="#">Systems</a></li>
									<li><a href="#">Devices</a></li>
								</ul>
								<br>

								<span class="filter-name">POPULAR</span>
								<ul class="myclass">
									<li><a href="#">Popular</a></li>
									<li><a href="#">Lone Star</a></li>
									<li><a href="#">Mobius</a></li>
								</ul>
							</div>
						</div>
					</div>
					
					<div class="col-9 col-tb-8 col-mb-12">
						<div class="products-all">
							<div class="products">
								<div class="overlay"><a href="#">view <br> product</a></div>
								<div class="product-inner">
									<div>
										<img src="img/products/1.jpg">
									</div>
									<div class="names">
										<a href="#" class="product-name">Cervical Sizer</a>
									</div>
								</div>	
							</div>
							<div class="products">
								<div class="overlay"><a href="#">view <br> product</a></div>
								<div class="product-inner">
									<div>
										<img src="img/products/2.jpg">
									</div>
									<div class="names">
										<a href="#">HUMI (Harris-Kronner) <br> Uterine Manipulator/ <br> Injector</a>
									</div>
								</div>	
							</div>
							<div class="products">
								<div class="overlay"><a href="#">view <br> product</a></div>
								<div class="product-inner">
									<div>
										<img src="img/products/3.jpg">
									</div>
									<div class="names">
										<a href="#">SeeClear Surgical <br> Smoke Evacuation System</a>
									</div>
								</div>	
							</div>
							<div class="products">
								<div class="overlay"><a href="#">view <br> product</a></div>
								<div class="product-inner">
									<div>
										<img src="img/products/4.jpg">
									</div>
									<div class="names">
										<a href="#">Apple-Hunt Laparascopic <br> Cannula/Trocar</a>
									</div>
								</div>	
							</div>
							<div class="products">
								<div class="overlay"><a href="#">view <br> product</a></div>
								<div class="product-inner">
									<div>
										<img src="img/products/5.jpg">
									</div>
									<div class="names">
										<a href="#">Z-Clamp Hysterectomy <br> Clamps</a>
									</div>
								</div>	
							</div>
							<div class="products">
								<div class="overlay"><a href="#">view <br> product</a></div>
								<div class="product-inner">
									<div>
										<img src="img/products/6.jpg">
									</div>
									<div class="names">
										<a href="#">Sacrocolpopexy <br> and Sacrocervicopexy Tips</a>
									</div>
								</div>	
							</div>
							<div class="products">
								<div class="overlay"><a href="#">view <br> product</a></div>
								<div class="product-inner">
									<div>
										<img src="img/products/7.jpg">
									</div>
									<div class="names">
										<a href="#">Cervical Sizer</a>
									</div>
								</div>	
							</div>
							<div class="products">
								<div class="overlay"><a href="#">view <br> product</a></div>
								<div class="product-inner">
									<div>
										<img src="img/products/8.jpg">
									</div>
									<div class="names">
										<a href="#">Endo-Sock Laparoscopic <br> Retrieval Pouch</a>
									</div>
								</div>	
							</div>
							<div class="products">
								<div class="overlay"><a href="#">view <br> product</a></div>
								<div class="product-inner">
									<div>
										<img src="img/products/3.jpg">
									</div>
									<div class="names">
										<a href="#">SeeClear Surgical <br> Smoke Evacuation System</a>
									</div>
								</div>	
							</div>
						</div>
					</div>

					<div class="page-list col-6">
						<a class="active" href="#">PREVIOUS</a>
						<a href="#">1</a>
						<a href="#">2</a>
						<a href="#">3</a>
						<a href="#">4</a>
						<a href="#">5</a>
						<a class="active" href="#">NEXT</a>
					</div>

				</div>
			</div>
		</div>	
	</div>

<?php include('footer.php'); ?>
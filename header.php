
<?php
	$host = (((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS']!=='off') || $_SERVER['SERVER_PORT']==443) ? 'https://':'http://' ).$_SERVER['HTTP_HOST'];
	$path = str_replace($_SERVER["DOCUMENT_ROOT"],'', str_replace('\\','/',__DIR__ ) );

	$documet_path = $host.$path;
?>
<!doctype html>
<html lang="en-US" class="no-js">
	<head>
		<meta charset="UTF-8">
		<title>Welcome</title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    	<link href="img/favicon-kanmed.png" rel="shortcut icon">	
		<!--<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js'></script>
    	<script type="text/javascript" src="js/jquery.themepunch.plugins.min.js"></script>
    	<script type="text/javascript" src="js/jquery.themepunch.revolution.js"></script>
    	<script type="text/javascript" src="js/scripts.js"></script>-->
    	<link rel="stylesheet" type="text/css" href="styles/dist/main.min.css">
    	<link rel="stylesheet" type="text/css" href="styles/css/slider.css">
    	<link rel="stylesheet" type="text/css" href="styles/css/settings.css">
    	<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<script type="text/javascript" src="js/dist/main.min.js"></script>
    	<script type="text/javascript">
    	var base = "<?php echo $documet_path;  ?>";
    	</script>
    	
	</head>
	<body>
		<div class="wrapper">
			<header class="header-section">
				<div class="logo-header">
					<div class="container">
						<div class="col-4 col-tb-3 col-mb-10">

							<div class="logo-cols ">
								<a class="logo" href="">
									<img src="img/logo-kanmed.svg" alt="">
								</a>
							</div>

						</div>
						<div class="col-mb-2 view_moblie navigation_mobile">
							<span id="nav-btn" class="mobile-defult-btn mobile-btn">
			                  <span class="top"></span>
			                  <span class="middle"></span>
			                  <span class="bottom"></span>
			                </span>
						</div>
						<div class="col-8 col-tb-9 col-mb-9">
						<div class="row">

							<div class="contact-cols">
							<ul>
								<li><i class="socail fb"></i><i class="socail twitter"></i><i class="socail google"></i></li>
								<li><i class="contact-icon phone"></i><strong>Call us - </strong> 0112 157 357</li>
							</ul>
						</div>


						<div class="nav-header">
						<nav class="navigation ">
							<ul>
								<li class="active"><a href="index.php">Our Company</a></li>
								<li><a href="products.php">Our Business</a></li>
								<li>
									<a href="partner.php">Our Partners</a>
									
								</li>
								<li><a href="business.php">Our Process</a></li>
								<li><a href="contact.php">Contacts us</a></li>
							</ul>
						</nav>
						</div>


						</div>
						

						
						</div>
					</div>
				</div>
			</header>
<?php include('header.php'); ?>

<!-- ###################### -->		
	<!-- Cover section -->
<!-- ###################### -->	

	<div class="cover-section other-section">
		<div class="about-section">
			<div class="container">
				<h1>About Our Company</h1>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem <br>
				 Ipsum has been the industry's standard dummy text ever since the 1500s, when an <br>
				 unknown printer took a galley of type and scrambled it to make a type </p>	
			</div>
		</div>
	</div>


	<div class="body-section">
	<!-- ###################### -->		
		<!-- who we are  -->
	<!-- ###################### -->

		<div class="row">
			<div class="container">
				<div class="container-wrap center">
					<div class="about-paragraph">
						<h1>WHO WE ARE</h1>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco <br> 
							laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non <br> proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,<br> 
							totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim <br>
							ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem <br> ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.Lorem ipsum dolor sit amet, <br> 
							consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea <br> 
							commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt <br> 
							mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, </p>
					</div>	
				</div>
			</div>
		</div>

	<!-- ###################### -->		
		<!-- List section -->
	<!-- ###################### -->

		<div class="row">
			<div class="container">
				<div class="container-wrap">
					
					<div class="tab">
						<ul class="tab-section">
							<li class="active">
								<a href="#content1" class="tablink">OUR MISSION</a>
							</li>
							<li>
								<a href="#content2" class="tablink">OUR VISION</a>
							</li>
						</ul>

						<!-- Contect of Tabs -->
						<div class="tab-content">
							<div class="content" id="content1">
	  							<div class="col-6 col-tb-12 col-mb-12">
	  								<div class="image-wrap">
	  									<img src="img/about/1.jpg" alt="image">
	  								</div>
	  							</div>
	  							<div class="col-6 col-tb-12 col-mb-12">
	  								<div class="image-para">
	  									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. <br> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, 

										totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.Lorem ipsum dolor sit amet, consectetur adipisicing elit, <br> 

										sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, </p>
	  								</div>
	  							</div>
							</div>

							<div class="content" id="content2">
	  							<div class="col-6 col-tb-12 col-mb-12">
	  								<div class="image-wrap">
	  									<img src="img/about/2.jpg" alt="image">
	  								</div>
	  							</div>
	  							<div class="col-6 col-tb-12 col-mb-12">
	  								<div class="image-para">
	  									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. <br> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, 

										totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.Lorem ipsum dolor sit amet, consectetur adipisicing elit, <br> 

										sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, </p>
	  								</div>
	  							</div>
							</div>

						</div>

					</div>
				</div>	
			</div>
		</div>

	<!-- ###################### -->		
		<!-- our values -->
	<!-- ###################### -->

		<div class="row">
			<div class="container">
				<div class="container-wrap">
					<div>
						<h1>OUR VALUES</h1>
						<div class="value-wrap">
							<div class="icon-values">
								<span class="icon-details icon1"></span>
								<span class="icon-font">Lorem Ipsum</span>
								<p>Lorem Ipsum dolor sit amet, consectetur adi
									pisicing elit, sed do eiusmod tempor incidi dunt ut labore et dolore </p>
							</div>
							<div class="icon-values">
								<span class="icon-details icon2"></span>
								<span class="icon-font">Lorem Ipsum</span>
								<p>Lorem Ipsum dolor sit amet, consectetur adi
									pisicing elit, sed do eiusmod tempor incidi dunt ut labore et dolore </p>
							</div>
							<div class="icon-values">
								<span class="icon-details icon3"></span>
								<span class="icon-font">Lorem Ipsum</span>
								<p>Lorem Ipsum dolor sit amet, consectetur adi
									pisicing elit, sed do eiusmod tempor incidi dunt ut labore et dolore </p>
							</div>
							<div class="icon-values">
								<span class="icon-details icon4"></span>
								<span class="icon-font">Lorem Ipsum</span>
								<p>Lorem Ipsum dolor sit amet, consectetur adi
									pisicing elit, sed do eiusmod tempor incidi dunt ut labore et dolore </p>
							</div>
							<div class="icon-values">
								<span class="icon-details icon5"></span>
								<span class="icon-font">Lorem Ipsum</span>
								<p>Lorem Ipsum dolor sit amet, consectetur adi
									pisicing elit, sed do eiusmod tempor incidi dunt ut labore et dolore </p>
							</div>
							<div class="icon-values">
								<span class="icon-details icon6"></span>
								<span class="icon-font">Lorem Ipsum</span>
								<p>Lorem Ipsum dolor sit amet, consectetur adi
									pisicing elit, sed do eiusmod tempor incidi dunt ut labore et dolore </p>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>


	</div>

<?php include('footer.php'); ?>
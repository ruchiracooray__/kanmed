		<footer class="footer-section">
				<div class="row">
					<div class="container">

						<div class="col-mb-12 view_moblie">
						<div class="footer_top_mobile">
						<img class="logo-footer" src="img/logo-kanmed.svg" alt="">
						<p>Lorem ipsum dolor sit amet, co secadipisicing elit, sed do eiusmod tempor incididunt ut labore et magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea </p>
						</div>
						</div>
						<div class="col-3 col-tb-6 col-mb-12">
							<div>
								<h3>About us</h3>
								<ul class='hide'>
									<li><a href="#">Tags</a></li>
									<li><a href="#">Support</a></li>
									<li><a href="#">Contact</a></li>
									<li><a href="#">How to credit the authorship of the icons?</a></li>
									<li><a href="#">Terms & conditions</a></li>
									<li><a href="#">Copyright notification</a></li>
									<li><a href="#">Privacy policy</a></li>
								</ul>
							</div>
						</div>
						<div class="col-3 col-tb-6 col-mb-12">
							<div>
								<h3>Contact us</h3>
								<p class="contact-us hide">
									Kanmed <br>
									No. <span>35/1</span>  Sumeda Mawatha, <br>
									Walpola New Mulleriyawawa Town- <span></span>, <br>
									Sri Lanka.
									<br>
									Telephone: <br>
									<span>+94-0112157357</span>
									<br>
									Fax: <br>
									<span>+94-0112157357</span>
									<br>
									Email: <br>
									info@kanmed.lk
								</p>
							</div>
						</div>
						<div class="col-3 col-tb-6 col-mb-12">
							<h3>Follow us</h3>
							<div class="hide">
							<div>
								

								<ul class="social-icon">
									<li><a href="#" class="icon-social icon-social-twitter"></a></li>
									<li><a href="#" class="icon-social icon-social-facebook"></a></li>
									<li><a href="#" class="icon-social icon-social-gplus"></a></li>
								</ul>
							</div>
							<div>
								<p>
									Terms and Conditions  Privacy  Disclaimer  Useful Links
								</p>
								<img class="qr-code" src="img/qr_code.jpg" alt="">
							</div>
						</div>
						</div>
						<div class="col-3 col-tb-6 col-mb-12">
							<div>
								<h3>STAY IN TOUCH WITH</h3>
								<div class='hide'>

								<p>
									Sign up for the Kanmed newsletter and be the first to know about news, videos, product updates, and more. You can unsubscribe at any time.
								</p>
							<div>
								<h3>Newsletter</h3>
								<p>
									Signup for the monthly newsletter to get usefull information for your 
								</p>
								<form class="subscribe" action="">
									<input type="text" placeholder="Email Address">
									<button>Sign up</button>
								</form>
								</div>
								</div>

							</div>
							<div class="text-right no_mobile">
								<img class="logo-footer" src="img/logo-kanmed.svg" alt="">
								<p class="copy-right">
									&copy Kanmed's, All Rights Reserved
								</p>
							</div>
						</div>
					</div>
				</div>
			</footer>